<?php

/**
 * Plugin Name: UTM Clicks
 * Description: Log visits with UTM parameters.
 * Version: 1.0.0
 * Author: Ebbinger Webdesign
 * Author URI: https://ebbinger.com
 * Text Domain: eweb-utm-clicks
 * 
 * Requires at least: 6.2
 * Requires PHP: 7.4.3
 * 
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 */

if (!defined('ABSPATH')) {
    exit;
}

define('EWEB_UTM_CLICKS_TABLE_NAME', 'eweb_utm_clicks');

// Set up database when activating the plugin
function eweb_uc_create_custom_table()
{
    global $wpdb;

    $table_name = $wpdb->prefix . EWEB_UTM_CLICKS_TABLE_NAME;

    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE IF NOT EXISTS $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        utm_source varchar(255) NOT NULL,
        utm_medium varchar(255),
        utm_campaign varchar(255),
        page_url varchar(255),
        click_date datetime NOT NULL,
        PRIMARY KEY (id)
    ) $charset_collate;";
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}
register_activation_hook(__FILE__, 'eweb_uc_create_custom_table');

// Recognize and handle utm links
function eweb_uc_check_utm_parameters()
{
    if (isset($_GET['utm_source'])) {
        $utm_source = sanitize_text_field($_GET['utm_source']);
        $utm_medium = isset($_GET['utm_medium']) ? sanitize_text_field($_GET['utm_medium']) : '';
        $utm_campaign = isset($_GET['utm_campaign']) ? sanitize_text_field($_GET['utm_campaign']) : '';
        $page = home_url($wp->request);

        eweb_uc_save_click_data($utm_source, $utm_medium, $utm_campaign, $page);
    }
}
add_action('wp_head', 'eweb_uc_check_utm_parameters');

// Save a set of click data in the database
function eweb_uc_save_click_data($utm_source, $utm_medium, $utm_campaign, $page)
{
    global $wpdb;

    $table_name = $wpdb->prefix . EWEB_UTM_CLICKS_TABLE_NAME;

    $wpdb->insert(
        $table_name,
        array(
            'utm_source' => $utm_source,
            'utm_medium' => $utm_medium,
            'utm_campaign' => $utm_campaign,
            'page_url' => $page,
            'click_date' => current_time('mysql'),
        )
    );
}

// Add admin page
function eweb_uc_add_custom_admin_page()
{
    add_submenu_page(
        'tools.php', // parent
        'UTM Clicks', // page title
        'UTM Clicks', // menu title
        'manage_options', // required capability
        'utm-clicks', // page slug
        'eweb_uc_display_utm_clicks_page', // content callback
        20 // menu position
    );
}
add_action('admin_menu', 'eweb_uc_add_custom_admin_page');

// Callback for admin page content
function eweb_uc_display_utm_clicks_page()
{
    global $wpdb;

    $page_url = admin_url('admin.php');
    $page_param = isset($_GET['page']) ? '&page=' . sanitize_text_field($_GET['page']) : '';


    $table_name = $wpdb->prefix . EWEB_UTM_CLICKS_TABLE_NAME;

    if (isset($_GET['utm_filter']) && $_GET['utm_filter'] != '') {
        $filter = sanitize_text_field($_GET['utm_filter']);
        // Show filtered clicks
        $clicks = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT * FROM $table_name WHERE utm_source=%s OR utm_medium=%s OR utm_campaign=%s",
                $filter,
                $filter,
                $filter
            )
        );
    } else {
        // Show all clicks
        $clicks = $wpdb->get_results("SELECT * FROM $table_name");
    }
    $count = $wpdb->num_rows;

    // Output
    echo '<div class="wrap eweb_utm_clicks">';
    echo '<h1>UTM Clicks</h1>';

    // Filter Form
    echo '<form method="get" action="' . esc_url($page_url . '?page=utm-clicks' . $page_param) . '">';
    echo '<input type="hidden" id="page" name="page" value="utm-clicks">';
    echo '<label for="filter">Filter: </label>';
    echo '<input type="text" id="filter" name="filter" value="' . ((isset($_GET['filter'])) ? htmlspecialchars($_GET['filter']) : '') . '" placeholder="source / medium / campaign">';
    echo '<button type="submit" class="button">Filter anwenden</button>';
    echo '</form>';

    // Results Table
    echo '<p class="results_count">' . esc_html($count) . ' results:</p>';

    echo '<table class="wp-list-table widefat striped">';
    echo '<thead>
            <tr>
                <th>#</th>
                <th>UTM Source</th>
                <th>UTM Medium</th>
                <th>UTM Campaign</th>
                <th>Page URL</th>
                <th>Click Date</th>
            </tr>
        </thead>';
    echo '<tbody>';
    $index = 1;
    foreach ($clicks as $click) {
        echo '<tr data-id="' . $click->id . '">';
        echo '<td>' . $index . '</td>';
        echo '<td>' . $click->utm_source . '</td>';
        echo '<td>' . $click->utm_medium . '</td>';
        echo '<td>' . $click->utm_campaign . '</td>';
        echo '<td>' . $click->page_url . '</td>';
        echo '<td>' . $click->click_date . '</td>';
        echo '</tr>';
        $index++;
    }
    echo '</tbody>
    </table>
</div>';
}

// Link logging page in plugin list
function eweb_uc_add_plugin_action_links($links)
{
    $utm_clicks_link = '<a href="tools.php?page=utm-clicks">UTM Log</a>';
    array_unshift($links, $utm_clicks_link);
    return $links;
}
add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'eweb_uc_add_plugin_action_links');
