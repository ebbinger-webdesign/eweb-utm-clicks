# EWEB UTM Clicks

A WordPress Plugin for logging website visits from UTM links.

## Features

- Log page visits that contain UTM parameters
- Does not store cookies
- View and filter logged visits in the dashboard

## Usage

Modify the URL of a page by including [UTM parameters](https://en.wikipedia.org/wiki/UTM_parameters#UTM_parameters):
```
https://example.com/?utm_source=newsletter&utm_medium=email&utm_campaign=test
```

Share the modified link. Any clicks will be logged and can be viewed and filtered in the dashboard (Tools > UTM Clicks).